from django import template

register = template.Library()


@register.filter(name='add_css_class')
def add_css_class(value, arg):
    return value.as_widget(attrs={'class': arg})


@register.filter
def messages_new_tags(messages):
    _map = {'error': 'danger'}
    return _map.get(messages.tags, messages.tags)
