from django.conf.urls import url

from user.views import user_login_view, user_register_view

urlpatterns = [
    url(r'^$', user_login_view, name='user_login_view'),
    url(r'^register/$', user_register_view, name='user_register_view'),
]
