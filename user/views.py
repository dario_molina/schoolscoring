from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy
from django.views.generic import FormView

from user.forms import LoginForm, RegisterForm
from user.models import User


class UserLoginView(FormView):
    template_name = 'user/login.html'
    form_class = LoginForm
    success_url = reverse_lazy('dashboard_view')

    def form_valid(self, form):
        login_user = form.cleaned_data
        user = authenticate(username=login_user.get('username'), password=login_user.get('password'))
        if user:
            if user.is_active:
                login(self.request, user)
                return super(UserLoginView, self).form_valid(form)
        return super(UserLoginView, self).form_invalid(form)


class UserRegisterView(FormView):
    template_name = 'user/register.html'
    form_class = RegisterForm
    success_url = reverse_lazy('dashboard_view')

    def form_valid(self, form):
        data = form.cleaned_data
        new_user = User.objects.create_user(
            username=data.get('username'),
            email=data.get('email'),
            password=data.get('password'),
            first_name=data.get('first_name'),
            last_name=data.get('last_name'),
        )
        login(self.request, new_user)
        return super(UserRegisterView, self).form_valid(form)


def logOut(request):
    logout(request)
    return reverse_lazy('user_login_view')


user_login_view = UserLoginView.as_view()
user_register_view = UserRegisterView.as_view()
