from django import forms
from django.contrib.auth import password_validation
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _

from user.models import User


class LoginForm(forms.Form):
    error_message = {
        'user_not_exist': _(u'User not exist'),
        'username_inactive': _(u'This user is inactive'),
        'incorrect_password': _(u'This password is incorrect'),
    }
    username = forms.CharField(
        label=_("Username"),
        required=True,
        validators=[RegexValidator('^[\\w.@+-]+$', _(u'Enter a valid user'), 'invalid')],
        widget=forms.TextInput(attrs={'placeholder': _('Username'), 'autofocus': 'autofocus'})
    )
    password = forms.CharField(
        label=_("Password"),
        required=True,
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder': _('Password')}),
        help_text=password_validation.password_validators_help_text_html(),
    )

    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')
        user = User.objects.filter(username=username)
        if not user.exists():
            self.add_form_errors(field='username', message='user_not_exist')
        else:
            if not user.first().is_active:
                self.add_form_errors(field='username', message='username_inactive')
            else:
                if not user.first().check_password(password):
                    self.add_form_errors(field='password', message='incorrect_password')
        return cleaned_data

    def add_form_errors(self, field, message):
        self.add_error(
            field=field,
            error=forms.ValidationError(self.error_message[message], code=message)
        )


class RegisterForm(forms.ModelForm):
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
        'username_already_exist': _("The username already exist."),
    }
    username = forms.CharField(
        label=_("Username"),
        required=True,
        min_length=8,
        validators=[RegexValidator('^[\\w.@+-]+$', _(u'Enter a valid user'), 'invalid')],
        widget=forms.TextInput(attrs={'placeholder': _('Username')})
    )
    first_name = forms.CharField(
        label=_("First Name"),
        required=True,
        min_length=4,
        validators=[RegexValidator('^[\\w.@+-]+$', _(u'Enter a valid first name'), 'invalid')],
        widget=forms.TextInput(attrs={'placeholder': _('First Name'), 'autofocus': 'autofocus'})
    )
    last_name = forms.CharField(
        label=_("Last Name"),
        required=True,
        min_length=4,
        validators=[RegexValidator('^[\\w.@+-]+$', _(u'Enter a valid last name'), 'invalid')],
        widget=forms.TextInput(attrs={'placeholder': _('Last Name')})
    )
    email = forms.CharField(
        label=_("Email"),
        required=False,
        widget=forms.TextInput(attrs={'placeholder': _('Email (No required)')})
    )
    password = forms.CharField(
        label=_("Password"),
        required=True,
        strip=False,
        widget=forms.PasswordInput(attrs={'placeholder': _('Password')}),
        help_text=password_validation.password_validators_help_text_html(),
    )
    password2 = forms.CharField(
        label=_("Password confirmation"),
        required=True,
        widget=forms.PasswordInput(attrs={'placeholder': _('Confirm password')}),
        strip=False,
        help_text=_("Enter the same password as before, for verification."),
    )

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password']

    def clean_password2(self):
        password = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password and password2 and password != password2:
            self.add_form_errors('password2', 'password_mismatch')
        return password2

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        username = cleaned_data.get('username')
        if username:
            if User.objects.filter(username=username).exists():
                self.add_form_errors('username', 'username_already_exist')
        return cleaned_data

    def add_form_errors(self, field, message):
        self.add_error(
            field=field,
            error=forms.ValidationError(self.error_messages[message], code=message)
        )
