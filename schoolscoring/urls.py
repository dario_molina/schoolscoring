from django.conf.urls import url
from django.contrib import admin
from django.urls import include

urlpatterns = [
    url(r'^select2/', include('django_select2.urls')),
    url('admin/', admin.site.urls),
    url('^', include('user.urls')),
    url('dashboard/', include('dashboard.urls')),
    url('game/', include('game.urls')),
]
