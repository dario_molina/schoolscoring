from schoolscoring.settings.base import *

# Database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'schoolscoring',
        'USER': 'schoolscoring',
        'PASSWORD': 'schoolscoring',
        'HOST': 'localhost',
        'PORT': 5432
    }
}

# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
