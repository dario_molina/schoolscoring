from django.conf.urls import url

from game.views import category_list_view, create_team_view, team_list_view, create_match_view

urlpatterns = [
    url(r'^category-list/$', category_list_view, name='category_list_view'),
    url(r'^create-team/$', create_team_view, name='create_team_view'),
    url(r'^team-list/(?P<pk>\d+)/$', team_list_view, name='team_list_view'),
    url(r'^create-match/$', create_match_view, name='create_match_view'),
]
