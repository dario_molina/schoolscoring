from django.contrib import admin

from game.models import Institution, Team, Category, Score, Match, Player, TeamMatch


@admin.register(Institution)
class InstitutionAdmin(admin.ModelAdmin):
    list_display = ['name', 'address', 'city', 'department', 'province']


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ['name', 'institution']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(Match)
class MatchInstitutionAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(TeamMatch)
class TeamMatchAdmin(admin.ModelAdmin):
    pass


@admin.register(Score)
class ScoreAdmin(admin.ModelAdmin):
    list_display = ['team', 'played', 'victories', 'tied', 'lost']


@admin.register(Player)
class PlayerAdmin(admin.ModelAdmin):
    list_display = ['name', 'team']
