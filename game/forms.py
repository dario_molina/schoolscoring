from django import forms
from django.utils.translation import ugettext_lazy as _
from django_select2.forms import Select2Widget

from game.models import Team, Institution, Category, Match, TeamMatch


class TeamForm(forms.ModelForm):

    name = forms.CharField(
        label=_(u'Name'),
        required=True,
        max_length=350,
        widget=forms.TextInput(attrs={'placeholder': _('Team Name'), 'autofocus': 'autofocus'})
    )
    institution = forms.ModelChoiceField(
        label=_(u'Institution'),
        queryset=Institution.objects.all(),
        required=True,
        widget=Select2Widget
    )
    category = forms.ModelChoiceField(
        label=_(u'Category'),
        queryset=Category.objects.all(),
        required=True,
        widget=Select2Widget
    )

    class Meta:
        model = Team
        fields = ['name', 'institution', 'category']


class MatchForm(forms.Form):
    error_message = {
        'exist_team_date': _(u'This team {} already has a match on the date number {}.'),
        'equal_teams': _(u'The teams are the same ({}).'),
    }
    team1 = forms.ModelChoiceField(
        label=_(u'Team 1'),
        required=True,
        queryset=Team.objects.all(),
        widget=Select2Widget
    )
    team2 = forms.ModelChoiceField(
        label=_(u'Team 2'),
        required=True,
        queryset=Team.objects.all(),
        widget=Select2Widget
    )
    date_number = forms.IntegerField(
        label=_(u'Date Number'),
        min_value=1,
        max_value=99,
        required=True,
    )

    def clean(self):
        clean = super(MatchForm, self).clean()
        if clean.get('team1') == clean.get('team2'):
            self.add_error(
                field='team2',
                error=forms.ValidationError(
                    self.error_message['equal_teams'].format(clean.get('team2')),
                    code='equal_teams')
            )
        team_match = TeamMatch.objects.filter(match__date_number=clean.get('date_number'))
        if team_match.filter(team=clean.get('team1')).exists():
            self.add_error(
                field='team1',
                error=forms.ValidationError(
                    self.error_message['exist_team_date'].format(clean.get('team1'), clean.get('date_number')),
                    code='exist_team_date')
            )
        if team_match.filter(team=clean.get('team2')).exists():
            self.add_error(
                field='team2',
                error=forms.ValidationError(
                    self.error_message['exist_team_date'].format(clean.get('team2'), clean.get('date_number')),
                    code='exist_team_date')
            )
        return clean
