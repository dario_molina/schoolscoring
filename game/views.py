from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import F
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _
from django.views.generic import ListView, FormView

from game.forms import TeamForm, MatchForm
from game.models import Category, Team, Score


class CategoryListView(ListView):
    template_name = 'game/tables/categories_list.html'
    model = Category

    def get_queryset(self):
        qs = Category.objects.all().values('id', 'name')
        return qs


class CreateTeamView(FormView):
    template_name = 'game/forms/create_team.html'
    form_class = TeamForm
    success_url = reverse_lazy('create_team_view')

    def form_valid(self, form):
        team = form.save()
        Score.objects.create(team=team)
        msg = _("Team save")
        messages.success(self.request, msg)
        return super(CreateTeamView, self).form_valid(form)


class TeamListView(ListView):
    template_name = 'game/tables/team_list.html'
    model = Team

    def get_queryset(self):
        category_id = self.kwargs.get('pk')
        qs = Team.objects.filter(
            category_id=category_id
        ).select_related(
            'scores'
        ).annotate(
            total_score=(F('scores__victories') * 3) + F('scores__tied'),
            played=F('scores__played'),
            victories=F('scores__victories'),
            tied=F('scores__tied'),
            lost=F('scores__lost'),
            goals=F('scores__total_points'),
            points_diff=F('scores__points_difference'),
            points_against=F('scores__points_against'),
        ).values(
            'name',
            'total_score',
            'played',
            'victories',
            'tied',
            'lost',
            'goals',
            'points_diff',
            'points_against',
        )
        return qs


class MatchView(FormView):
    template_name = 'game/forms/create_match.html'
    form_class = MatchForm

    def form_valid(self, form):
        data = form.cleaned_data
        return super(MatchView, self).form_valid(form)


category_list_view = CategoryListView.as_view()
team_list_view = TeamListView.as_view()
# Staff member
create_team_view = staff_member_required(CreateTeamView.as_view())
create_match_view = staff_member_required(MatchView.as_view())
