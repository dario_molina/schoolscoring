from django.db import models
from django.utils.translation import gettext_lazy as _
from django_extensions.db.models import TimeStampedModel


class Institution(TimeStampedModel):
    name = models.CharField(_(u'Name'), max_length=350)
    address = models.CharField(_(u'Address'), max_length=350, blank=True)
    city = models.CharField(_(u'City'), max_length=350, blank=True)
    department = models.CharField(_(u'Department'), max_length=350, blank=True)
    province = models.CharField(_(u'Province'), max_length=350, blank=True)
    phone = models.CharField(_(u'phone'), max_length=12, blank=True)

    def __str__(self):
        return '{}'.format(self.name)


class Team(TimeStampedModel):
    name = models.CharField(_(u'Name'), max_length=350)
    institution = models.ForeignKey(
        'game.Institution', related_name='teams', blank=True, null=True, on_delete=models.SET_NULL
    )
    category = models.ForeignKey(
        'game.Category', related_name='teams', blank=True, null=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        return '{} - {}'.format(self.name, self.category)


class Category(TimeStampedModel):
    name = models.CharField(_(u'Category'), max_length=100)

    def __str__(self):
        return '{}'.format(self.name)


class Match(TimeStampedModel):
    name = models.CharField(_(u'Name'), max_length=700)
    date_number = models.PositiveIntegerField(_(u'Date number'), default=1)

    def __str__(self):
        return '{} - {}'.format(self.name, self.date_number)


class TeamMatch(TimeStampedModel):
    team = models.ForeignKey(
        'game.Team', related_name='team_matches', blank=True, null=True, on_delete=models.SET_NULL
    )
    match = models.ForeignKey(
        'game.Match', related_name='team_matches', blank=True, null=True, on_delete=models.SET_NULL
    )
    points = models.PositiveIntegerField(_(u'Points'), default=0)

    def __str__(self):
        return '{} - {}'.format(self.team, self.match)


class Score(TimeStampedModel):
    team = models.OneToOneField('game.Team', related_name='scores', on_delete=models.CASCADE)

    # Games
    played = models.PositiveIntegerField(_(u'Played'), default=0)
    victories = models.PositiveIntegerField(_(u'Victories'), default=0)
    tied = models.PositiveIntegerField(_(u'Tied'), default=0)
    lost = models.PositiveIntegerField(_(u'Lost'), default=0)

    # Goals
    total_points = models.PositiveIntegerField(_(u'Total points'), default=0)
    points_difference = models.PositiveIntegerField(_(u'Points difference'), default=0)
    points_against = models.PositiveIntegerField(_(u'Points against'), default=0)


class Player(TimeStampedModel):
    name = models.CharField(_(u'Name'), max_length=350)
    team = models.ForeignKey('game.Team', related_name='players', on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.name)
