from django.db.models import F, Sum
from django.views.generic import ListView

from game.models import Institution


class DashboardView(ListView):
    model = Institution
    template_name = 'game/tables/total_by_institution.html'

    def get_queryset(self):
        qs = Institution.objects.all().prefetch_related(
            'teams__scores'
        ).annotate(
            total=Sum(F('teams__scores__victories'))
        ).values(
            'name',
            'total'
        )
        return qs


dashboard_view = DashboardView.as_view()
